package ca.pfv.spmf.algorithms;

import java.io.IOException;
import java.io.OutputStream;

import ca.pfv.spmf.patterns.Itemsets;

public interface Algorithm<M, N extends Comparable<M>> {

	Itemsets<M, N> runAlgorithm(double minsup, String input, OutputStream output)
			throws IOException;

	void printStats();

	int getDatabaseSize();

	void reset();
	
	void doAdditionalOutput(OutputStream os);
}
