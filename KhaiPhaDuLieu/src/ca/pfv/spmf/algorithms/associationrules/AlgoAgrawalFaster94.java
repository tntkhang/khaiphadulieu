package ca.pfv.spmf.algorithms.associationrules;

import java.io.IOException;
import java.io.OutputStream;

import ca.pfv.spmf.patterns.Itemsets;

public interface AlgoAgrawalFaster94<M, N extends Comparable<M>, T extends AssocRule<M, N>> {
	/**
	 * Run the algorithm
	 * 
	 * @param patterns
	 *            a set of frequent itemsets
	 * @param output
	 *            an output file path for writing the result or null if the user
	 *            want this method to return the result
	 * @param databaseSize
	 *            the number of transactions in the database
	 * @param minconf
	 *            the minconf threshold
	 * @return the set of association rules if the user wished to save them into
	 *         memory
	 * @throws IOException
	 *             exception if error writing to the output file
	 */
	<K extends Itemsets<M, N>> AssocRules<M, N, T> runAlgorithm(K patterns, OutputStream output, int databaseSize,
			double minconf) throws IOException;

	/**
	 * Convert a rule to string
	 * 
	 * @param rule
	 *            Origin rule
	 * @return
	 */
	String ruleToString(T rule);
}
