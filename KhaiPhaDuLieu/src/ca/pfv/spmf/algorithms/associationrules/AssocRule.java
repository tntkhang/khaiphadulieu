package ca.pfv.spmf.algorithms.associationrules;

import ca.pfv.spmf.patterns.Rule;

public interface AssocRule<M, N extends Comparable<M>> extends Rule<M, N> {

	/**
	 * Get the lift of this rule.
	 * 
	 * @return the lift.
	 */
	double getLift();

}
