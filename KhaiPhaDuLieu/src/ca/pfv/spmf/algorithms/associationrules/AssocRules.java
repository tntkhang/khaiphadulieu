package ca.pfv.spmf.algorithms.associationrules;

import ca.pfv.spmf.patterns.Rules;

public interface AssocRules<M, N extends Comparable<M>, T extends AssocRule<M, N>>
		extends Rules<M, N, T> {

}
