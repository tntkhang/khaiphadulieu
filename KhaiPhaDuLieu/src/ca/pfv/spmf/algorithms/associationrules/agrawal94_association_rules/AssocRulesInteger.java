package ca.pfv.spmf.algorithms.associationrules.agrawal94_association_rules;

/* This file is copyright (c) 2008-2012 Philippe Fournier-Viger
 * 
 * This file is part of the SPMF DATA MINING SOFTWARE
 * (http://www.philippe-fournier-viger.com/spmf).
 * 
 * SPMF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * SPMF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * SPMF. If not, see <http://www.gnu.org/licenses/>.
 */

import ca.pfv.spmf.algorithms.associationrules.AssocRule;
import ca.pfv.spmf.algorithms.associationrules.AssocRules;
import ca.pfv.spmf.patterns.AbstractRules;

/**
 * This class represents a list of association rules found by the Agrawal
 * algorithm for association rule mining. It is used for storing the rules found
 * by the algorithm, when the user choose to keep the result into memory rather
 * than saving it to a file.
 * 
 * @see AlgoAgrawalFaster94Integer
 * @see AssocRuleInteger
 * @author Philippe Fournier-Viger
 */

public class AssocRulesInteger extends
		AbstractRules<Integer, Integer, AssocRuleInteger> implements
		AssocRules<Integer, Integer, AssocRuleInteger> {

	/**
	 * Constructor
	 * 
	 * @param name
	 *            a name for this list of association rules (string)
	 */
	public AssocRulesInteger(String name) {
		this.name = name;
	}

	/**
	 * Print all the rules in this list to System.out, and include information
	 * about the lift measure.
	 * 
	 * @param databaseSize
	 *            the number of transactions in the transaction database where
	 *            the rules were found
	 */
	public void printRulesWithLift(int databaseSize) {
		System.out.println(" ------- " + name + " -------");
		int i = 0;
		for (AssocRule<Integer, Integer> rule : rules) {
			System.out.print("  rule " + i + ":  " + rule.toString());
			System.out.print("support :  "
					+ rule.getRelativeSupport(databaseSize) + " ("
					+ rule.getAbsoluteSupport() + "/" + databaseSize + ") ");
			System.out.print("confidence :  " + rule.getConfidence());
			System.out.print(" lift :  " + rule.getLift());
			System.out.println("");
			i++;
		}
		System.out.println(" --------------------------------");
	}
}
