package ca.pfv.spmf.algorithms.frequentpatterns.fpgrowth;

import ca.pfv.spmf.algorithms.Algorithm;

public interface AloFpGrowth<M, N extends Comparable<M>> extends
		Algorithm<M, N> {

	public FPTree<M, N> getFpTree();
}
