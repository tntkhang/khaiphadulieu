package ca.pfv.spmf.algorithms.frequentpatterns.fpgrowth;

import java.util.List;

public interface FPTree<M, T extends Comparable<M>> {

	/**
	 * Method for adding a transaction to the fp-tree (for the initial
	 * construction of the FP-Tree).
	 * 
	 * @param transaction
	 */
	void addTransaction(List<T> transaction);

	/**
	 * Get root node.
	 * 
	 * @return Root node
	 */
	FPNode<M, T> getRootNode();
}
