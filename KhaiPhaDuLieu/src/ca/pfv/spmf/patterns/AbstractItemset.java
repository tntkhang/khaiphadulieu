package ca.pfv.spmf.patterns;

public abstract class AbstractItemset<M, T extends Comparable<M>> implements Itemset<M, T> {

	/** the array of items **/
	public T[] itemset;

	/**
	 * Get the items as array
	 * 
	 * @return the items
	 */
	public T[] getItems() {
		return itemset;
	}

	/**
	 * Get the size of this itemset
	 */
	@Override
	public int size() {
		return itemset.length;
	}

	/**
	 * Get the item at a given position in this itemset
	 */
	public T get(int position) {
		return itemset[position];
	}

	/**
	 * Get this itemset as a string
	 * 
	 * @return a string representation of this itemset
	 */
	public String toString() {
		// use a string buffer for more efficiency
		StringBuffer r = new StringBuffer();
		// for each item, append it to the stringbuffer
		for (int i = 0; i < size(); i++) {
			r.append(get(i));
			r.append(' ');
		}
		return r.toString(); // return the tring
	}
}
