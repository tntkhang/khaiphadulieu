package ca.pfv.spmf.patterns;

public class AbstractRule<M, T extends Comparable<M>> implements Rule<M, T> {

	/** antecedent */
	protected T[] itemset1;
	/** consequent */
	protected T[] itemset2;
	/** coverage (support of the antecedent) */
	protected int coverage;
	/** relative support */
	protected int transactionCount;
	/** confidence of the rule */
	protected double confidence;

	/**
	 * Print this rule to System.out.
	 */
	public void print() {
		System.out.println(toString());
	}

	/**
	 * Get the left itemset of this rule (antecedent).
	 * 
	 * @return an itemset.
	 */
	public T[] getItemset1() {
		return itemset1;
	}

	/**
	 * Get the right itemset of this rule (consequent).
	 * 
	 * @return an itemset.
	 */
	public T[] getItemset2() {
		return itemset2;
	}

	/**
	 * Get the relative support of the rule (percentage)
	 * 
	 * @param databaseSize
	 *            the number of transactions in the database where this rule was
	 *            found.
	 * @return the support (double)
	 */
	public double getRelativeSupport(int databaseSize) {
		return ((double) transactionCount) / ((double) databaseSize);
	}

	/**
	 * Get the absolute support of this rule (integer).
	 * 
	 * @return the absolute support.
	 */
	public int getAbsoluteSupport() {
		return transactionCount;
	}

	/**
	 * Get the confidence of this rule.
	 * 
	 * @return the confidence
	 */
	public double getConfidence() {
		return confidence;
	}

	/**
	 * Get the coverage of the rule (the support of the rule antecedent) as a
	 * number of transactions
	 * 
	 * @return the coverage (int)
	 */
	public int getCoverage() {
		return coverage;
	}
}
