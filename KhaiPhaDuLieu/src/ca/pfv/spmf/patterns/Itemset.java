package ca.pfv.spmf.patterns;


public interface Itemset<M, T extends Comparable<M>> {

	/**
	 * Get the size of this itemset
	 * 
	 * @return the size of this itemset
	 */
	int size();

	/**
	 * Get this itemset as a string
	 * 
	 * @return a string representation of this itemset
	 */
	String toString();

	/**
	 * print this itemset to System.out.
	 */
	void print();

	/**
	 * Get the support of this itemset
	 * 
	 * @return the support of this itemset
	 */
	int getAbsoluteSupport();

	/**
	 * Get the relative support of this itemset (a percentage) as a double
	 * 
	 * @param nbObject
	 *            the number of transactions in the database where this itemset
	 *            was found
	 * @return the relative support of the itemset as a double
	 */
	double getRelativeSupport(int nbObject);

	/**
	 * Check if this itemset contains a given item.
	 * 
	 * @param item
	 *            the item
	 * @return true if the item is contained in this itemset
	 */
	boolean contains(T item);

	/**
	 * Get the item at a given position of this itemset
	 * 
	 * @param position
	 *            the position of the item to be returned
	 * @return the item
	 */
	T get(int position);

	/**
	 * This method compare this itemset with another itemset to see if they are
	 * equal. The method assume that the two itemsets are lexically ordered.
	 * 
	 * @param an
	 *            itemset
	 * @return true or false
	 */
	boolean isEqualTo(T[] itemset);

	/**
	 * Get the items as array
	 * 
	 * @return the items
	 */
	T[] getItems();
}
