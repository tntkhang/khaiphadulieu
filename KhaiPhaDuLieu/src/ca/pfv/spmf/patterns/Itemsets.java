package ca.pfv.spmf.patterns;

public interface Itemsets<M, N extends Comparable<M>> {

    /**
     * Add an itemset to this structure
     * 
     * @param itemset the itemset
     * @param k the number of items contained in the itemset
     */
    <T extends Itemset<M, N>> void addItemset(T itemset, int k);

    /**
     * @return Number of itemsets
     */
    int countItemsets();
}
