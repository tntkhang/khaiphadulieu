package ca.pfv.spmf.patterns;


public interface Rule<M, T extends Comparable<M>> {

	/**
	 * Get the confidence of this rule.
	 * 
	 * @return the confidence
	 */
	double getConfidence();

	/**
	 * Get the relative support of the rule (percentage)
	 * 
	 * @param databaseSize
	 *            the number of transactions in the database where this rule was
	 *            found.
	 * @return the support (double)
	 */
	double getRelativeSupport(int databaseSize);

	/**
	 * Get the absolute support of this rule (integer).
	 * 
	 * @return the absolute support.
	 */
	int getAbsoluteSupport();

	/**
	 * Print this rule to System.out.
	 */
	void print();

	/**
	 * Get the left itemset of this rule (antecedent).
	 * 
	 * @return an itemset.
	 */
	T[] getItemset1();

	/**
	 * Get the right itemset of this rule (consequent).
	 * 
	 * @return an itemset.
	 */
	T[] getItemset2();

	/**
	 * Get the coverage of the rule (the support of the rule antecedent) as a
	 * number of transactions
	 * 
	 * @return the coverage (int)
	 */
	int getCoverage();
}
