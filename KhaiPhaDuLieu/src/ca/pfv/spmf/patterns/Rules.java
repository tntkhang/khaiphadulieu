package ca.pfv.spmf.patterns;

import java.util.List;

public interface Rules<M, N extends Comparable<M>, T extends Rule<M, N>> {

	/**
	 * Print all the rules in this list to System.out.
	 * 
	 * @param databaseSize
	 *            the number of transactions in the transaction database where
	 *            the rules were found
	 */
	void printRules(int databaseSize);

	/**
	 * Sort the rules by confidence
	 */
	void sortByConfidence();

	/**
	 * Return a string representation of this list of rules
	 * 
	 * @param databaseSize
	 *            the number of transactions in the database where the rules
	 *            were found.
	 * @return a string
	 */
	String toString(int databaseSize);

	/**
	 * Add a rule to this list of rules
	 * 
	 * @param rule
	 *            the rule to be added
	 */
	void addRule(T rule);

	/**
	 * Get the number of rules in this list of rules
	 * 
	 * @return the number of rules
	 */
	int getRulesCount();

	/**
	 * Get the list of rules.
	 * 
	 * @return a list of rules.
	 */
	List<T> getRules();
}
