package ca.pfv.spmf.patterns.itemset_array_integers_with_count;

import ca.pfv.spmf.algorithms.ArraysAlgos;
import ca.pfv.spmf.patterns.AbstractOrderedIntegerItemset;
import ca.pfv.spmf.patterns.Itemset;

/* This file is copyright (c) 2008-2012 Philippe Fournier-Viger
 * 
 * This file is part of the SPMF DATA MINING SOFTWARE
 * (http://www.philippe-fournier-viger.com/spmf).
 * 
 * SPMF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * SPMF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * SPMF. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class represents an itemset (a set of items) implemented as an array of
 * integers with a variable to store the support count of the itemset.
 * 
 * @author Philippe Fournier-Viger
 */
public class ItemsetIntegerWithCount extends AbstractOrderedIntegerItemset
		implements Itemset<Integer, Integer> {

	/** the support of this itemset */
	public int support = 0;

	/**
	 * Constructor
	 */
	public ItemsetIntegerWithCount() {
		itemset = new Integer[] {};
	}

	/**
	 * Constructor
	 * 
	 * @param item
	 *            an item that should be added to the new itemset
	 */
	public ItemsetIntegerWithCount(Integer item) {
		itemset = new Integer[] { item };
	}

	/**
	 * Constructor
	 * 
	 * @param items
	 *            an array of items that should be added to the new itemset
	 */
	public ItemsetIntegerWithCount(Integer[] items) {
		this.itemset = items;
	}

	/**
	 * Get the support of this itemset
	 */
	public int getAbsoluteSupport() {
		return support;
	}

	/**
	 * Set the support of this itemset
	 * 
	 * @param support
	 *            the support
	 */
	public void setAbsoluteSupport(Integer support) {
		this.support = support;
	}

	/**
	 * Increase the support of this itemset by 1
	 */
	public void increaseTransactionCount() {
		this.support++;
	}

	/**
	 * Make a copy of this itemset but exclude a given item
	 * 
	 * @param itemToRemove
	 *            the given item
	 * @return the copy
	 */
	public ItemsetIntegerWithCount cloneItemSetMinusOneItem(Integer itemToRemove) {
		// create the new itemset
		Integer[] newItemset = new Integer[itemset.length - 1];
		int i = 0;
		// for each item in this itemset
		for (int j = 0; j < itemset.length; j++) {
			// copy the item except if it is the item that should be excluded
			if (itemset[j] != itemToRemove) {
				newItemset[i++] = itemset[j];
			}
		}
		return new ItemsetIntegerWithCount(newItemset); // return the copy
	}

	/**
	 * Make a copy of this itemset but exclude a set of items
	 * 
	 * @param itemsetToNotKeep
	 *            the set of items to be excluded
	 * @return the copy
	 */
	public ItemsetIntegerWithCount cloneItemSetMinusAnItemset(
			ItemsetIntegerWithCount itemsetToNotKeep) {
		// create a new itemset
		Integer[] newItemset = new Integer[itemset.length
				- itemsetToNotKeep.size()];
		int i = 0;
		// for each item of this itemset
		for (int j = 0; j < itemset.length; j++) {
			// copy the item except if it is not an item that should be excluded
			if (itemsetToNotKeep.contains(itemset[j]) == false) {
				newItemset[i++] = itemset[j];
			}
		}
		return new ItemsetIntegerWithCount(newItemset); // return the copy
	}

	/**
	 * This method return an itemset containing items that are included in this
	 * itemset and in a given itemset
	 * 
	 * @param itemset2
	 *            the given itemset
	 * @return the new itemset
	 */
	public ItemsetIntegerWithCount intersection(ItemsetIntegerWithCount itemset2) {
		Integer[] intersection = ArraysAlgos.intersectTwoSortedArrays(
				this.getItems(), itemset2.getItems());
		return new ItemsetIntegerWithCount(intersection);
	}
}
