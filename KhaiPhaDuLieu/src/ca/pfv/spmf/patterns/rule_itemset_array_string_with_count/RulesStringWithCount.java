package ca.pfv.spmf.patterns.rule_itemset_array_string_with_count;

/* This file is copyright (c) 2008-2012 Philippe Fournier-Viger
 * 
 * This file is part of the SPMF DATA MINING SOFTWARE
 * (http://www.philippe-fournier-viger.com/spmf).
 * 
 * SPMF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * SPMF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * SPMF. If not, see <http://www.gnu.org/licenses/>.
 */

import ca.pfv.spmf.patterns.AbstractRules;

/**
 * This class represents a list of association rules, where itemsets are array
 * of integers.
 * 
 * @see RuleStringWithCount
 * @author Philippe Fournier-Viger
 */

public class RulesStringWithCount extends
		AbstractRules<String, String, RuleStringWithCount> {

	/**
	 * Constructor
	 * 
	 * @param name
	 *            a name for this list of association rules (string)
	 */
	public RulesStringWithCount(String name) {
		this.name = name;
	}

}
