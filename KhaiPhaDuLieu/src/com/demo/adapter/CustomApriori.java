package com.demo.adapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.StringTokenizer;
import java.util.Vector;

import ca.pfv.spmf.algorithms.Algorithm;
import ca.pfv.spmf.patterns.AbstractOrderedStringItemset;
import ca.pfv.spmf.patterns.itemset_array_strings_with_count.ItemsetStringWithCount;
import ca.pfv.spmf.patterns.itemset_array_strings_with_count.ItemsetsStringWithCount;

public class CustomApriori extends ItemsetsStringWithCount implements
		Algorithm<String, String> {

	public CustomApriori() {
		super("Custom Apriori");
	}

	Vector<String> candidates = new Vector<String>(); // candidates
	int numItems; // so item moi transactions
	int numTransactions; // so transactions
	double minsup;
	String oneVal[]; // mang danh so '1' de so sanh
	Vector<String> temCi = new Vector<String>();// danh sach Ci va tan so xuat
												// hien
	Vector<String> frequentCandidates1 = new Vector<String>();// danh sach FI

	@Override
	public CustomApriori runAlgorithm(double minsup, String input,
			OutputStream output) throws IOException {
		this.minsup = minsup;
		BufferedReader br = Files.newBufferedReader(FileSystems.getDefault()
				.getPath(input), Charset.forName("UTF-8"));
		String line;
		int level = 0;
		while ((line = br.readLine()) != null) {
			// if the line is a comment, is empty or is a
			// kind of metadata
			if (line.isEmpty() == true || line.charAt(0) == '#'
					|| line.charAt(0) == '%' || line.charAt(0) == '@') {
				continue;
			}
			// split the line according to spaces
			String[] lineSplited = line.split(" ");

			// create an array of int to store the items in this transaction
			String transaction[] = new String[lineSplited.length];

			// for each item in this line (transaction)
			for (int i = 0; i < lineSplited.length; i++) {
				// store the item in the memory representation of the database
				transaction[i] = lineSplited[i];
			}
			numItems = transaction.length;
			ItemsetStringWithCount itemset = new ItemsetStringWithCount(
					transaction);
			this.addItemset(itemset, level++);
		}
		br.close();
		numTransactions = level;
		oneVal = new String[numItems];
		for (int i = 0; i < oneVal.length; i++) {
			oneVal[i] = "1";
		}
		int itemsetNumber = 0;
		do {
			itemsetNumber++; // so phan tu Li
			// tao candidates
			generateCandidates(itemsetNumber);
			calculateFrequentItemsets(itemsetNumber);
		} while (candidates.size() > 1);

		return this;
	}

	@Override
	public void printStats() {
		// DO nothing
	}

	@Override
	public int getDatabaseSize() {
		return numTransactions;
	}

	@Override
	public void reset() {
		candidates = new Vector<String>(); // candidates
		numItems = 0; // so item moi transactions
		numTransactions = 0; // so transactions
		minsup = 0;
		oneVal = null;
		temCi = new Vector<String>();// danh sach Ci va tan so xuat hien
		frequentCandidates1 = new Vector<String>();// danh sach FI
	}

	private void generateCandidates(int n) {// tạo candidates
		Vector<String> tempCandidates = new Vector<String>(); // temporary
																// candidate
		String str1, str2;
		StringTokenizer st1, st2; // string tokenizers để so sánh 2 phần
									// tử
		// Candidate có 1 phần tử
		if (n == 1) {
			for (int i = 1; i <= numItems; i++) {
				tempCandidates.add(Integer.toString(i));
			}
		} else if (n == 2) // Candidate có 2 phần tử
		{
			// kết hợp với phần tử có trong Candidate 1 phần tử
			for (int i = 0; i < candidates.size(); i++) {
				st1 = new StringTokenizer(candidates.get(i));
				str1 = st1.nextToken();
				for (int j = i + 1; j < candidates.size(); j++) {
					st2 = new StringTokenizer(candidates.elementAt(j));
					str2 = st2.nextToken();
					tempCandidates.add(str1 + " " + str2);
				}
			}
		} else {// candidate có n phần tử (n>= 3)
			// duyệt Candidates
			for (int i = 0; i < candidates.size(); i++) {
				// so sánh với phần tử tiếp theo,nếu giống n-2
				// phần tử đầu thì
				// kết hợp 2 phần tử cuối
				for (int j = i + 1; j < candidates.size(); j++) {
					// tạo 2 string mới
					str1 = new String();
					str2 = new String();
					// tạo 2 Stringtokenizer để lấy n-2 phần tử
					// đầu
					st1 = new StringTokenizer(candidates.get(i));
					st2 = new StringTokenizer(candidates.get(j));
					// tạo string mới có n-2 phần tử
					for (int s = 0; s < n - 2; s++) {
						str1 = str1 + " " + st1.nextToken();
						str2 = str2 + " " + st2.nextToken();
					}
					// nếu giống nhau, thì add vào
					if (str2.compareToIgnoreCase(str1) == 0) {
						tempCandidates
								.add((str1 + " " + st1.nextToken() + " " + st2
										.nextToken()).trim());
					}
				}
			}
		}
		candidates.clear();
		candidates = new Vector<String>(tempCandidates);
		tempCandidates.clear();
	}

	private void calculateFrequentItemsets(int n) throws IOException {
		Vector<String> frequentCandidates = new Vector<String>();
		StringTokenizer st;
		boolean match;
		boolean trans[] = new boolean[numItems];
		int count[] = new int[candidates.size()];
		// duyet het trans:
		for (int i = 0; i < numTransactions; i++) {
			// doc CSDL
			AbstractOrderedStringItemset itemSet = this.getLevels().get(i)
					.get(0);
			for (int j = 0; j < numItems; j++) {
				System.out.println();
				trans[j] = (itemSet.get(j).compareToIgnoreCase(oneVal[j]) == 0);
			}
			// kiểm tra mỗi phần tử trong itemset với transaction
			// nếu không đúng thì break,nếu đùng thì tăng count lên
			for (int c = 0; c < candidates.size(); c++) {
				match = false;
				st = new StringTokenizer(candidates.get(c));
				while (st.hasMoreTokens()) {
					match = (trans[Integer.valueOf(st.nextToken()) - 1]);
					if (!match) {
						break;
					}
				}
				if (match) {
					count[c]++;
				}
			}
		}
		// lưu support và FI lại
		for (int i = 0; i < candidates.size(); i++) {
			temCi.add(candidates.get(i) + ":" + count[i]);
			if ((count[i] / (double) numTransactions) >= minsup) {
				frequentCandidates.add(candidates.get(i));
				frequentCandidates1.add(candidates.get(i));
			}
		}
		candidates.clear();
		candidates = new Vector<String>(frequentCandidates);
		frequentCandidates.clear();
	}

	@Override
	public void doAdditionalOutput(OutputStream os) {
		// Nothing more to ouput
		try {
			os.flush();
		} catch (IOException e) {
			// DO nothing
		}
	}
}
