package com.demo.gui;

import ca.pfv.spmf.algorithms.Algorithm;
import ca.pfv.spmf.algorithms.associationrules.AlgoAgrawalFaster94;
import ca.pfv.spmf.algorithms.associationrules.AssocRule;

public class AlgorithmComboboxItem<M, N extends Comparable<M>, I extends AssocRule<M, N>> {

	private byte algoId;
	private String algoLabel;
	private Algorithm<M, N> algorithm;
	private AlgoAgrawalFaster94<M, N, I> agrawalFaster94;

	public AlgorithmComboboxItem(byte algoId, String algoLabel,
			Algorithm<M, N> algorithm,
			AlgoAgrawalFaster94<M, N, I> agrawalFaster94) {
		this.algoId = algoId;
		this.algoLabel = algoLabel;
		this.algorithm = algorithm;
		this.agrawalFaster94 = agrawalFaster94;
	}

	public byte getAlgoId() {
		return algoId;
	}

	public void setAlgoId(byte algoId) {
		this.algoId = algoId;
	}

	public String getAlgoLabel() {
		return algoLabel;
	}

	public void setAlgoLabel(String algoLabel) {
		this.algoLabel = algoLabel;
	}

	@Override
	public String toString() {
		return algoLabel;
	}

	public Algorithm<M, N> getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(Algorithm<M, N> algorithm) {
		this.algorithm = algorithm;
	}

	public AlgoAgrawalFaster94<M, N, I> getAgrawalFaster94() {
		return agrawalFaster94;
	}

	public void setAgrawalFaster94(AlgoAgrawalFaster94<M, N, I> agrawalFaster94) {
		this.agrawalFaster94 = agrawalFaster94;
	}

}
