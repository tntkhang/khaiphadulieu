package com.demo.gui;

import ca.pfv.spmf.algorithms.associationrules.agrawal94_association_rules.AlgoAgrawalFaster94Integer;
import ca.pfv.spmf.algorithms.associationrules.agrawal94_association_rules.AssocRuleInteger;
import ca.pfv.spmf.algorithms.associationrules.agrawal94_string_association_rules.AlgoAgrawalFaster94String;
import ca.pfv.spmf.algorithms.associationrules.agrawal94_string_association_rules.AssocRuleString;
import ca.pfv.spmf.algorithms.frequentpatterns.apriori.AlgoApriori;
import ca.pfv.spmf.algorithms.frequentpatterns.apriori.string.AlgoAprioriString;
import ca.pfv.spmf.algorithms.frequentpatterns.fpgrowth_with_strings.AlgoFPGrowthStrings;

import com.demo.adapter.CustomApriori;

public class AlgorithmConst {

	public static final AlgorithmComboboxItem<Integer, Integer, AssocRuleInteger> APRIORI_ITEM = new AlgorithmComboboxItem<Integer, Integer, AssocRuleInteger>(
			(byte) 1, "Apriori", new AlgoApriori(),
			new AlgoAgrawalFaster94Integer());

	public static final AlgorithmComboboxItem<String, String, AssocRuleString> FPGROWTH_ITEM = new AlgorithmComboboxItem<String, String, AssocRuleString>(
			(byte) 2, "FPGrowth", new AlgoFPGrowthStrings(),
			new AlgoAgrawalFaster94String());

	public static final AlgorithmComboboxItem<String, String, AssocRuleString> CUSTOM_APRIORI_ITEM = new AlgorithmComboboxItem<String, String, AssocRuleString>(
			(byte) 3, "Apriori", new CustomApriori(),
			new AlgoAgrawalFaster94String());

	public static final AlgorithmComboboxItem<String, String, AssocRuleString> APRIORI_STRING_ITEM = new AlgorithmComboboxItem<String, String, AssocRuleString>(
			(byte) 4, "Apriori", new AlgoAprioriString(),
			new AlgoAgrawalFaster94String());
}
