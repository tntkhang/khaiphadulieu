package com.demo.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ConfigPanel extends JPanel {

	/**
	 * The serial version uid.
	 */
	private static final long serialVersionUID = 5001111921069055793L;

	// Input components
	private JLabel inputFileLabel = new JLabel("Input file:");
	private JLabel minsupLabel = new JLabel("Choose minsup (0.4 is 40%):");
	private JLabel minconfLabel = new JLabel("Choose minconf (0.4 is 40%):");

	private JTextField inputFileTextField = new JTextField();
	private JButton browseInputFileButton = new JButton("Browse...");
	private JTextField minsupTextField = new JTextField("0.5");
	private JTextField minconfTextField = new JTextField("0.6");
	// Configuration components
	private JLabel configFileLabel = new JLabel("Config:");
	private JTextField configFileTextField = new JTextField();
	private JButton browseConfigFileButton = new JButton("Browse...");

	public ConfigPanel() {
		int labelWidth = 150;
		setLayout(new GridLayout(0, 1, 0, 0));
		// Select file panel
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 5));
		inputFileLabel.setPreferredSize(new Dimension(labelWidth, 35));
		panel.add(inputFileLabel);
		JPanel selectFilePanel = new JPanel(new BorderLayout());
		selectFilePanel.add(inputFileTextField);
		inputFileTextField.setPreferredSize(new Dimension(200, 35));
		selectFilePanel.add(browseInputFileButton, BorderLayout.EAST);
		panel.add(selectFilePanel);
		add(panel);
		// Minsup
		panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 5));
		minsupLabel.setPreferredSize(new Dimension(labelWidth, 35));
		panel.add(minsupLabel);
		panel.add(minsupTextField);
		minsupTextField.setHorizontalAlignment(JTextField.RIGHT);
		minsupTextField.setPreferredSize(new Dimension(278, 35));
		add(panel);
		// Minconf
		panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 5));
		minconfLabel.setPreferredSize(new Dimension(labelWidth, 35));
		panel.add(minconfLabel);
		panel.add(minconfTextField);
		minconfTextField.setHorizontalAlignment(JTextField.RIGHT);
		minconfTextField.setPreferredSize(new Dimension(278, 35));
		add(panel);
		// Config file panel
		panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 5));
		configFileLabel.setPreferredSize(new Dimension(labelWidth, 35));
		panel.add(configFileLabel);
		JPanel selectConfigFilePanel = new JPanel(new BorderLayout());
		selectConfigFilePanel.add(configFileTextField);
		configFileTextField.setPreferredSize(new Dimension(200, 35));
		selectConfigFilePanel.add(browseConfigFileButton, BorderLayout.EAST);
		panel.add(selectConfigFilePanel);
		add(panel);
	}

	public JTextField getInputFileTextField() {
		return inputFileTextField;
	}

	public JButton getBrowseInputFileButton() {
		return browseInputFileButton;
	}

	public JTextField getMinsupTextField() {
		return minsupTextField;
	}

	public JTextField getMinconfTextField() {
		return minconfTextField;
	}

	public JButton getBrowseConfigFileButton() {
		return browseConfigFileButton;
	}

	public JTextField getConfigFileTextField() {
		return configFileTextField;
	}

}
