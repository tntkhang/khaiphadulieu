package com.demo.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainFrame extends JFrame {

	/**
	 * Teh serial version uid.
	 */
	private static final long serialVersionUID = -7211830456259049694L;

	private MainPanel mainPanel;

	public MainFrame() {
		mainPanel = new MainPanel();
		setLayout(new BorderLayout(10, 10));
		add(mainPanel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Association rules");
		// add gap space
		add(new JPanel(), BorderLayout.NORTH);
		add(new JPanel(), BorderLayout.SOUTH);
		add(new JPanel(), BorderLayout.WEST);
		add(new JPanel(), BorderLayout.EAST);
	}

	public MainPanel getMainPanel() {
		return mainPanel;
	}

}
