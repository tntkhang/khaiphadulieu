package com.demo.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class MainPanel extends JPanel {

	/**
	 * The serial version ID.
	 */
	private static final long serialVersionUID = 5001111921069055793L;
	/**
	 * Config panel.
	 */
	private ConfigPanel configPanel;
	private JTextArea output1;
	private JScrollPane outputScroll1;
	private JTextArea output2;
	private JScrollPane outputScroll2;
	private JTextArea output3;
	private JScrollPane outputScroll3;

	private JButton aprioriRunButton = new JButton("Run Apriori:");
	private JButton aprioriOutputButton = new JButton("Read Output:");
	private JButton fpgrowthRunButton = new JButton("Run FP_Growth:");
	private JButton fpgrowOutputButton = new JButton("Read Output:");

	public MainPanel() {
		JPanel panel;
		setLayout(new BorderLayout(0, 0));
		JPanel headerPanel = new JPanel(new GridLayout(1, 2, 10, 0));
		add(headerPanel, BorderLayout.NORTH);
		/*
		 * Config panel.
		 */
		JPanel containerPanel = new JPanel(new BorderLayout(0, 0));
		configPanel = new ConfigPanel();
		panel = new JPanel(new BorderLayout());
		panel.add(configPanel, BorderLayout.NORTH);
		panel.add(new JPanel());
		containerPanel.add(panel, BorderLayout.CENTER);
		configPanel.setPreferredSize(new Dimension(500, 160));
		// Action for apriori
		JPanel aprioriActionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT,
				0, 10));
		containerPanel.add(aprioriActionPanel, BorderLayout.SOUTH);
		headerPanel.add(containerPanel);
		aprioriActionPanel.add(aprioriRunButton);
		aprioriActionPanel.add(aprioriOutputButton);
		/*
		 * Output 1.
		 */
		containerPanel = new JPanel(new BorderLayout(0, 0));
		output1 = new JTextArea();
		outputScroll1 = new JScrollPane(output1);
		outputScroll1.setPreferredSize(new Dimension(500, 250));
		containerPanel.add(outputScroll1, BorderLayout.CENTER);
		// Action for fpgroth
		JPanel fpgrothActionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT,
				0, 10));
		containerPanel.add(fpgrothActionPanel, BorderLayout.SOUTH);
		headerPanel.add(containerPanel);
		fpgrothActionPanel.add(fpgrowthRunButton);
		fpgrothActionPanel.add(fpgrowOutputButton);
		/*
		 * Output 2.
		 */
		JPanel outputCenter = new JPanel(new GridLayout(1, 2, 10, 0));
		output2 = new JTextArea();
		outputScroll2 = new JScrollPane(output2);
		outputCenter.add(outputScroll2);
		/*
		 * Output 3.
		 */
		output3 = new JTextArea();
		outputScroll3 = new JScrollPane(output3);
		outputCenter.add(outputScroll3);
		// Center
		add(outputCenter);
	}

	public JTextArea getOutput1() {
		return output1;
	}

	public JScrollPane getOutputScroll1() {
		return outputScroll1;
	}

	public JTextArea getOutput2() {
		return output2;
	}

	public JScrollPane getOutputScroll2() {
		return outputScroll2;
	}

	public JTextArea getOutput3() {
		return output3;
	}

	public JScrollPane getOutputScroll3() {
		return outputScroll3;
	}

	public ConfigPanel getConfigPanel() {
		return configPanel;
	}

	public JButton getAprioriRunButton() {
		return aprioriRunButton;
	}

	public JButton getAprioriOutputButton() {
		return aprioriOutputButton;
	}

	public JButton getFpgrowthRunButton() {
		return fpgrowthRunButton;
	}

	public JButton getFpgrowOutputButton() {
		return fpgrowOutputButton;
	}

}
