package com.demo.gui.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import ca.pfv.spmf.algorithms.Algorithm;
import ca.pfv.spmf.algorithms.associationrules.AlgoAgrawalFaster94;
import ca.pfv.spmf.algorithms.associationrules.agrawal94_string_association_rules.AssocRuleString;
import ca.pfv.spmf.patterns.Itemsets;

import com.demo.gui.AlgorithmComboboxItem;
import com.demo.gui.AlgorithmConst;
import com.demo.gui.ConfigPanel;
import com.demo.gui.MainFrame;
import com.demo.gui.MainPanel;
import com.demo.gui.listener.FileBrowseActionListener;
import com.demo.gui.listener.FileHandleAction;

public class MainController {

    private MainFrame mainFrame;
    private MainPanel mainPanel;
    private AlgorithmComboboxItem<String, String, AssocRuleString> aprioriAlgorithm = AlgorithmConst.APRIORI_STRING_ITEM;
    private AlgorithmComboboxItem<String, String, AssocRuleString> fpgrowthAlgorithm = AlgorithmConst.FPGROWTH_ITEM;
    private DecimalFormat df = new DecimalFormat("#%");

    public MainController() {
        mainFrame = new MainFrame();
        mainPanel = mainFrame.getMainPanel();
    }

    public void showMainWindow() {
        mainFrame.setSize(1024, 700);
        mainFrame.setVisible(true);
    }

    public void addListeners() {
        final ConfigPanel configPanel = mainPanel.getConfigPanel();
        configPanel.getBrowseInputFileButton().addActionListener(
                new FileBrowseActionListener(mainFrame, new FileHandleAction() {

                    @Override
                    public void doAction(File file) {
                        configPanel.getInputFileTextField().setText(file.getAbsolutePath());
                    }
                }));
        configPanel.getBrowseConfigFileButton().addActionListener(
                new FileBrowseActionListener(mainFrame, new FileHandleAction() {

                    @Override
                    public void doAction(File file) {
                        configPanel.getConfigFileTextField().setText(file.getAbsolutePath());
                    }
                }));
        mainPanel.getAprioriRunButton().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                Algorithm<String, String> algorithm = aprioriAlgorithm.getAlgorithm();
                runAlgorithm(algorithm, aprioriAlgorithm.getAgrawalFaster94());
            }
        });
        mainPanel.getFpgrowthRunButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                Algorithm<String, String> algorithm = fpgrowthAlgorithm.getAlgorithm();
                runAlgorithm(algorithm, fpgrowthAlgorithm.getAgrawalFaster94());
            }
        });
        mainPanel.getAprioriOutputButton().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                Algorithm<String, String> algorithm = aprioriAlgorithm.getAlgorithm();
                readOutput(algorithm, aprioriAlgorithm.getAgrawalFaster94());
            }
        });
        mainPanel.getFpgrowOutputButton().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                Algorithm<String, String> algorithm = fpgrowthAlgorithm.getAlgorithm();
                readOutput(algorithm, fpgrowthAlgorithm.getAgrawalFaster94());
            }
        });
    }

    private void runAlgorithm(Algorithm<String, String> algorithm,
            AlgoAgrawalFaster94<String, String, AssocRuleString> agrawalFaster94) {
        JTextArea output2 = null;
        if (algorithm.equals(AlgorithmConst.APRIORI_STRING_ITEM.getAlgorithm())) {
            output2 = mainPanel.getOutput2();
        } else {
            output2 = mainPanel.getOutput3();
        }
        try {
            ConfigPanel configPanel = mainPanel.getConfigPanel();
            double minsup = Double.parseDouble(configPanel.getMinsupTextField().getText());
            double minconf = Double.parseDouble(configPanel.getMinconfTextField().getText());
            String inputFilePath = configPanel.getInputFileTextField().getText();
            // Output 2
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            algorithm.reset();
            Itemsets<String, String> itemsets = algorithm.runAlgorithm(minsup, inputFilePath, null);
            int databaseSize = algorithm.getDatabaseSize();
            agrawalFaster94.runAlgorithm(itemsets, baos, databaseSize, minconf);
            String output = new String(baos.toByteArray());
            output2.setText(output);
            // Output 1
            baos = new ByteArrayOutputStream();
            algorithm.doAdditionalOutput(baos);
            output = new String(baos.toByteArray());
            mainPanel.getOutput1().setText(output);
        } catch (IOException e1) {
            e1.printStackTrace();
            JOptionPane.showMessageDialog(mainFrame, "Error", e1.getMessage(), JOptionPane.ERROR_MESSAGE);
        }
    }

    private void readOutput(Algorithm<String, String> algorithm,
            AlgoAgrawalFaster94<String, String, AssocRuleString> agrawalFaster94) {
        JTextArea output2 = null;
        if (algorithm.equals(AlgorithmConst.APRIORI_STRING_ITEM.getAlgorithm())) {
            output2 = mainPanel.getOutput2();
        } else {
            output2 = mainPanel.getOutput3();
        }
        try {
            ConfigPanel configPanel = mainPanel.getConfigPanel();
            double minsup = Double.parseDouble(configPanel.getMinsupTextField().getText());
            double minconf = Double.parseDouble(configPanel.getMinconfTextField().getText());
            String inputFilePath = configPanel.getInputFileTextField().getText();
            String configFilePath = configPanel.getConfigFileTextField().getText();
            Map<String, String> configData = readConfiguration(configFilePath);
            // Output 2
            algorithm.reset();
            Itemsets<String, String> itemsets = algorithm.runAlgorithm(minsup, inputFilePath, null);
            int databaseSize = algorithm.getDatabaseSize();
            List<AssocRuleString> assocRules = agrawalFaster94.runAlgorithm(itemsets, null, databaseSize, minconf)
                    .getRules();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);
            try {
                int index = 0;
                for (AssocRuleString assocRuleString : assocRules) {
                    String[] itemset1 = assocRuleString.getItemset1();
                    String[] itemset2 = assocRuleString.getItemset2();
                    applyConfig(itemset1, configData);
                    applyConfig(itemset2, configData);
                    ps.println(printConfiguredRule(assocRuleString, ++index, assocRules.size(), configData));
                }
            } finally {
                ps.close();
            }
            String output = new String(baos.toByteArray());
            output2.setText(output);
            // Output 1
            baos = new ByteArrayOutputStream();
            algorithm.doAdditionalOutput(baos);
            output = new String(baos.toByteArray());
            mainPanel.getOutput1().setText(output);
        } catch (IOException e1) {
            e1.printStackTrace();
            JOptionPane.showMessageDialog(mainFrame, "Error", e1.getMessage(), JOptionPane.ERROR_MESSAGE);
        }
    }

    private Map<String, String> readConfiguration(String configPath) throws IOException {
        Map<String, String> result = new HashMap<String, String>();
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(configPath), "UTF-8"));
        try {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains(":")) {
                    String key = line.substring(0, line.indexOf(":")).trim();
                    String value = line.substring(line.indexOf(":") + 1).trim();
                    result.put(key, value);
                }
            }
        } finally {
            br.close();
        }
        return result;
    }

    private void applyConfig(String[] itemset, Map<String, String> configData) {
        for (int i = 0; i < itemset.length; i++) {
            String item = itemset[i];
            String data = configData.get(item);
            if (data != null) {
                itemset[i] = data;
            }
        }
    }

    /**
     * Return a String representation of this rule
     * 
     * @return a String
     */
    public String printConfiguredRule(AssocRuleString assocRuleString, int itemIndex, int itemCount,
            Map<String, String> config) {
        String message = config.get("message");
        String andOper = config.get("and");
        StringBuffer buffer;
        // Index and count
        String itemIndexString = String.valueOf(itemIndex);
        String itemCountString = String.valueOf(itemCount);
        while (itemIndexString.length() < itemCountString.length()) {
            itemIndexString = "0" + itemIndexString;
        }
        message = message.replace("${index}", itemIndexString).replace("${count}", itemCountString);
        String[] itemset1 = assocRuleString.getItemset1();
        String[] itemset2 = assocRuleString.getItemset2();
        double conf = assocRuleString.getConfidence();
        // write itemset 1
        buffer = new StringBuffer();
        for (int i = 0; i < itemset1.length; i++) {
            buffer.append(itemset1[i]);
            if (i < itemset1.length - 2) {
                buffer.append(", ");
            } else if (i == itemset1.length - 2) {
                buffer.append(" " + andOper + " ");
            }
        }
        message = message.replace("${action1}", buffer.toString());
        // write itemset 2
        buffer = new StringBuffer();
        for (int i = 0; i < itemset2.length; i++) {
            buffer.append(itemset2[i]);
            if (i < itemset2.length - 2) {
                buffer.append(", ");
            } else if (i == itemset2.length - 2) {
                buffer.append(" " + andOper + " ");
            }
        }
        message = message.replace("${sequenseActionList}", buffer.toString());
        // Release buffer
        buffer = null;
        // Write conf
        message = message.replace("${percent}", df.format(conf));
        return message;
    }
}
