package com.demo.gui.listener;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

public class FileBrowseActionListener implements ActionListener {

	private static final JFileChooser FILE_CHOOSER = new JFileChooser();
	static {
	    try {
            File currentDir = new File("config.txt").getAbsoluteFile().getParentFile();
            FILE_CHOOSER.setCurrentDirectory(currentDir);
            System.out.println("Current dir: " + currentDir.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	private Component mainComponent;
	private FileHandleAction fileHandleAction;

	public FileBrowseActionListener(Component mainComponent,
			FileHandleAction fileHandleAction) {
		this.mainComponent = mainComponent;
		this.fileHandleAction = fileHandleAction;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		int dialogResult = FILE_CHOOSER.showOpenDialog(mainComponent);
		if (dialogResult == JFileChooser.APPROVE_OPTION) {
			File selectedFile = FILE_CHOOSER.getSelectedFile();
			if (fileHandleAction != null) {
				fileHandleAction.doAction(selectedFile);
			}
		}
	}

}
