package com.demo.gui.listener;

import java.io.File;

public interface FileHandleAction {

	/**
	 * Do action with the given file
	 * 
	 * @param file
	 *            The file param
	 */
	void doAction(File file);

}
