package mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import sosanh.LVKetNoiDatabase;

public class MySQLConnection {
    private static String sever;
    private static String user;
    private static String pass;
    private static String databaseName;
	public MySQLConnection() {}
	
	public static Connection getConnection() {


		Connection connection = null;
		//String host = "localhost:3306";
                if (LVKetNoiDatabase.serverDatabase != null) {
                    sever = LVKetNoiDatabase.serverDatabase;
                }
                
		//String user = "root";
                if (LVKetNoiDatabase.userDatabase != null) {
                    user = LVKetNoiDatabase.userDatabase;
                }
                
               
		//String pass = "admin";
                if (LVKetNoiDatabase.passDatabase != null) {
                    pass = LVKetNoiDatabase.passDatabase;
                }
                
		//String data = "bhxh";
                if (LVKetNoiDatabase.tableDatabase != null) {
                    databaseName = LVKetNoiDatabase.tableDatabase;
                }
                
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();                     
                        connection = DriverManager.getConnection("jdbc:mysql://" + sever + "/" + databaseName + "?useUnicode=true&characterEncoding=UTF-8" , user, pass);                     			
		} catch (Exception e) {
                }
		return connection;
	}
	public static String getChoi(){
           return LVKetNoiDatabase.userDatabase;
        }
	public static ResultSet getResultSet(String query) {
		ResultSet rs = null;
		try {
			Connection conn = getConnection();
			Statement stat = conn.createStatement();
			rs = stat.executeQuery(query);
		} catch (Exception e) {};
		return rs;
	}
	
	public static int update(String query) {
		int result = 0;
		try {
			Connection conn = getConnection();
			Statement stat = conn.createStatement();
			result = stat.executeUpdate(query);
		} catch (Exception e) {}
		return result;
	}
}
