/*
 * MainView.java
 */
package sosanh;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.Timer;

import org.jdesktop.application.Action;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The application's main frame.
 */
public class LVMainView extends FrameView {

    File fileInput;
    String pathCa;
    String transaFile;
    String outputFile;
    int numItems; //so item moi transactions
    int numTransactions; //so transactions
    double minSup; //do support tieu chuan
    double minCon;//do con tieu chuan
    String oneVal[]; //array of value per column that will be treated as a '1'
    String itemSep = " "; //the separator value for items in the database

    public LVMainView(SingleFrameApplication app) {
       
        super(app);

        initComponents();
        getFrame().setLocation((Toolkit.getDefaultToolkit().getScreenSize().width)/4 - getFrame().getWidth()/4, (Toolkit.getDefaultToolkit().getScreenSize().height)/4 - getFrame().getHeight()/4);
        getFrame().setResizable(false);
        
        getFrame().toFront();
        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");

        showPopupKetNoiCSDL();
    }

    

    @Action
    public void showAboutBox() {
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel(){
            ImageIcon icon = new ImageIcon("sr/sosanh/resources/splash.png");
            public void paintComponent(Graphics g){
                Dimension d = getSize();
                g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
                setOpaque(false);
                super.paintComponent(g);
            }
        }
        ;
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton(){
            ImageIcon icon = new ImageIcon("src/sosanh/resources/inst_process.png");
            public void paintComponent(Graphics g){
                Dimension d = getSize();
                g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
                setOpaque(false);
                super.paintComponent(g);
            }
        };
        javax.swing.JLabel appVersionLabel = new javax.swing.JLabel();
        javax.swing.JLabel versionLabel = new javax.swing.JLabel();
        javax.swing.JLabel vendorLabel = new javax.swing.JLabel();
        javax.swing.JLabel appVendorLabel = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        javax.swing.JLabel vendorLabel1 = new javax.swing.JLabel();
        javax.swing.JLabel appVendorLabel1 = new javax.swing.JLabel();
        javax.swing.JLabel vendorLabel2 = new javax.swing.JLabel();
        javax.swing.JLabel appVendorLabel2 = new javax.swing.JLabel();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(sosanh.LVApp.class).getContext().getResourceMap(LVMainView.class);
        mainPanel.setBackground(resourceMap.getColor("mainPanel.background")); // NOI18N
        mainPanel.setName("mainPanel"); // NOI18N

        jLabel3.setBackground(resourceMap.getColor("jLabel3.background")); // NOI18N
        jLabel3.setFont(resourceMap.getFont("jLabel3.font")); // NOI18N
        jLabel3.setForeground(resourceMap.getColor("jLabel3.foreground")); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel3.setIcon(resourceMap.getIcon("jLabel3.icon")); // NOI18N
        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N

        jButton1.setFont(resourceMap.getFont("jButton1.font")); // NOI18N
        jButton1.setIcon(resourceMap.getIcon("jButton1.icon")); // NOI18N
        jButton1.setText(resourceMap.getString("jButton1.text")); // NOI18N
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton1.setName("jButton1"); // NOI18N
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                buttonXulydulieu(evt);
            }
        });

        jButton2.setFont(resourceMap.getFont("jButton2.font")); // NOI18N
        jButton2.setForeground(resourceMap.getColor("jButton2.foreground")); // NOI18N
        jButton2.setIcon(resourceMap.getIcon("jButton2.icon")); // NOI18N
        jButton2.setText(resourceMap.getString("jButton2.text")); // NOI18N
        jButton2.setActionCommand(resourceMap.getString("jButton2.actionCommand")); // NOI18N
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton2.setName("jButton2"); // NOI18N
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                buttonRunThuatToan(evt);
            }
        });

        appVersionLabel.setFont(resourceMap.getFont("appVersionLabel.font")); // NOI18N
        appVersionLabel.setText(resourceMap.getString("appVersionLabel.text")); // NOI18N
        appVersionLabel.setName("appVersionLabel"); // NOI18N

        versionLabel.setFont(new java.awt.Font("DialogInput", 1, 18));
        versionLabel.setForeground(resourceMap.getColor("versionLabel.foreground")); // NOI18N
        versionLabel.setText(resourceMap.getString("versionLabel.text")); // NOI18N
        versionLabel.setName("versionLabel"); // NOI18N

        vendorLabel.setFont(new java.awt.Font("DialogInput", 1, 18));
        vendorLabel.setForeground(resourceMap.getColor("vendorLabel.foreground")); // NOI18N
        vendorLabel.setText(resourceMap.getString("vendorLabel.text")); // NOI18N
        vendorLabel.setName("vendorLabel"); // NOI18N

        appVendorLabel.setFont(resourceMap.getFont("appVendorLabel.font")); // NOI18N
        appVendorLabel.setText(resourceMap.getString("appVendorLabel.text")); // NOI18N
        appVendorLabel.setName("appVendorLabel"); // NOI18N

        jButton3.setFont(resourceMap.getFont("jButton3.font")); // NOI18N
        jButton3.setIcon(resourceMap.getIcon("jButton3.icon")); // NOI18N
        jButton3.setText(resourceMap.getString("jButton3.text")); // NOI18N
        jButton3.setActionCommand(resourceMap.getString("jButton3.actionCommand")); // NOI18N
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton3.setName("jButton3"); // NOI18N
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                buttonCloseClick(evt);
            }
        });

        vendorLabel1.setFont(resourceMap.getFont("vendorLabel1.font")); // NOI18N
        vendorLabel1.setForeground(resourceMap.getColor("vendorLabel1.foreground")); // NOI18N
        vendorLabel1.setText(resourceMap.getString("vendorLabel1.text")); // NOI18N
        vendorLabel1.setName("vendorLabel1"); // NOI18N

        appVendorLabel1.setFont(resourceMap.getFont("appVendorLabel1.font")); // NOI18N
        appVendorLabel1.setText(resourceMap.getString("appVendorLabel1.text")); // NOI18N
        appVendorLabel1.setName("appVendorLabel1"); // NOI18N

        vendorLabel2.setFont(resourceMap.getFont("vendorLabel2.font")); // NOI18N
        vendorLabel2.setForeground(resourceMap.getColor("vendorLabel2.foreground")); // NOI18N
        vendorLabel2.setText(resourceMap.getString("vendorLabel2.text")); // NOI18N
        vendorLabel2.setName("vendorLabel2"); // NOI18N

        appVendorLabel2.setFont(resourceMap.getFont("appVendorLabel2.font")); // NOI18N
        appVendorLabel2.setText(resourceMap.getString("appVendorLabel2.text")); // NOI18N
        appVendorLabel2.setName("appVendorLabel2"); // NOI18N

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 779, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(versionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(mainPanelLayout.createSequentialGroup()
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(vendorLabel1)
                                    .addComponent(vendorLabel)
                                    .addComponent(vendorLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(appVersionLabel, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(appVendorLabel, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(appVendorLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(appVendorLabel2, javax.swing.GroupLayout.Alignment.LEADING)))))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(versionLabel)
                    .addComponent(appVersionLabel))
                .addGap(6, 6, 6)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(vendorLabel)
                    .addComponent(appVendorLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(vendorLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(vendorLabel2))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(appVendorLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(appVendorLabel2)))
                .addGap(143, 143, 143))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addContainerGap(218, Short.MAX_VALUE)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        setComponent(mainPanel);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonRunThuatToan(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonRunThuatToan
        JDialog p1 = new LVGiaoDienThuatToan();
        LVApp.getApplication().show(p1);
        p1.setLocationRelativeTo(mainPanel);
}//GEN-LAST:event_buttonRunThuatToan

    private void buttonXulydulieu(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonXulydulieu
            JDialog p1 = new LVGiaoDienXuLyDL();
        LVApp.getApplication().show(p1);
    }//GEN-LAST:event_buttonXulydulieu

    private void showPopupKetNoiCSDL() {
        JDialog p1 = new LVKetNoiDatabase();
        LVApp.getApplication().show(p1);
    }
    
    private void buttonCloseClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonCloseClick
          System.exit(0);
    }//GEN-LAST:event_buttonCloseClick
    public void setConnect(String text) {
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel mainPanel;
    // End of variables declaration//GEN-END:variables
    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    private JDialog aboutBox;
}
